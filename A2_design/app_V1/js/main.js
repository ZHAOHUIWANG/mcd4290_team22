"use strict";

//Mapbox access token
mapboxgl.accessToken = ACCESS_TOKEN;

//define the map
let map;
    
    map = new mapboxgl.Map({
    container: "map",
    style: "mapbox://styles/mapbox/streets-v11",
    zoom: 4,
    center: [134.09209, -26.083647],
    });

map.addControl(
  new MapboxGeocoder({
    accessToken: mapboxgl.accessToken,
    mapboxgl: mapboxgl,
  })
);

let currentBookingStops = [];
let nthStop = 1;
let lat = 0;
let lng = 0;
let address = "";

//Get the current location of the user and show a popup & marker for usr current location
function getUserCurrentLocation() {
  if ("geolocation" in navigator) {
    navigator.geolocation.getCurrentPosition(success, error);
  } else {
    error();
  }
}

function success(position){
    lat = position.coords.latitude;
    lng = position.coords.longitude;
    
    sendWebServiceRequestForReverseGeocoding(lat, lng, "onAddressChanged");

    markPlaceOnMap();
}
 
function error(){
    alert("It requires user's permission to use this feature");
}

function markPlaceOnMap() {
    map.flyTo({
        center: [lng, lat],
        essential: true,
        zoom: 15,
    });

    setTimeout(function (e) {
        let popup = new mapboxgl.Popup({ closeOnClick: false })
        .setLngLat([lng, lat])
        .setHTML(
            `<p><strong>${address.substring(0,address.indexOf(","))}<br/></strong>${address}</p>
             <button id="stop${nthStop}" class="btnAdd" onclick="showAddress()">Add</button>`)
        .addTo(map);

        let marker = new mapboxgl.Marker({ draggable: true })
        .setLngLat([lng, lat])  
        .setPopup(popup)   
        .addTo(map)
        .togglePopup();

        function onDragEnd(){
            let lngLat = marker.getLngLat();
            lat = lngLat.lat;
            lng = lngLat.lng;

        
            console.log("Longitude: " + lngLat.lng + "<br />Latitude: " + lngLat.lat);

        
            sendWebServiceRequestForReverseGeocoding(lat, lng, "onAddressChanged");

        
            setTimeout(function(){
                popup.setHTML(
                    `<p><strong>${address.substring(0,address.indexOf(","))}</strong><br/>${address}</p>
                    <button id="stop${nthStop}" class="btnAdd">Add</button>`
                );
                
                document.getElementById(`input${nthStop}`).value = address;
          
                document
                    .getElementById(`stop${nthStop}`)
                    .addEventListener("click", function (e) {
                    onAddlLocation(lat, lng);
                    document.getElementById(`stop${nthStop}`).style.visibility = "hidden";});
            }, 1000);
        }
    
        marker.on("dragend", onDragEnd);

        document
            .getElementById(`stop${nthStop}`)
            .addEventListener("click", function(e){
                onAddlLocation(lat, lng);
                document.getElementById(`stop${nthStop}`).style.visibility = "hidden";
                // document.getElementById(`input${nthStop}`).value = address;
        });
    }, 1000);
}

function showAddress(){
    document.getElementById(`input${nthStop}`).value = address;
}

function onAddressChanged(response) {
    console.log(response);
    
    let result = response.results[0];
    lat = result.geometry.lat;
    lng = result.geometry.lng;
    let components = result.components;

    address =
        components.road === undefined
        ? ""
    :components.road + ", " + components.town === undefined
        ? ""
    :components.town +
        ", " +
        components.city +
        ", " +
        components.state +
        ", " +
        components.country +
        ", " +
        components.postcode;
}

function addStopToPage(n) {
    let divStops = document.getElementById("moreStops");
    // console.log(html);
    let newNode = document.createElement("div");
    newNode.id = "row" + n;
    newNode.innerHTML = `
        <div class="mdl-grid">
            <div style="display: table-row">
                <div style="display: table-cell;">
                    <input class="mdl-textfield__input input-stop" type="text" placeholder="Type or drag on the map" id="input${n}">
                </div>
                <div style="display: table-cell" >
                    <button onclick="searchLocation('input${n}')" class="mdl-button--fab mdl-button--mini-fab search-stop">
                        <i class="material-icons">search</i>
                    </button>
                    <button onclick="removeStopFromPage(${n})" class=" mdl-button--fab mdl-button--mini-fab delete-stop">
                        <i class="material-icons">delete</i>
                    </button>   
                </div>
            </div>
        </div>` ;
    divStops.appendChild(newNode);
}

function searchLocation(id) {
    console.log(id);
    let location = document.getElementById(id).value;
    
    console.log("search location " + location);
    sendWebServiceRequestForForwardGeocoding(location, "onLocationResponse");
}

function onLocationResponse(response) {
    // console.log(response);
    let result = response.results[0];
    let coordinates = result.bounds.northeast;
    
    address = result.formatted;
    lat = coordinates.lat;
    lng = coordinates.lng;

    markPlaceOnMap();
}

function onAddlLocation(lat, lng) {
    console.log(lat);
    console.log(lng);
    
    // let newStop = new Stop(name, address, lat, lng);
    let index = currentBookingStops.findIndex((b) => b._id == "stop" + nthStop);
    let stop = {
        _id: "stop" + nthStop,
        _name: address.substring(0, address.indexOf(",")),
        _address: address,
        _lat: lat,
        _lng: lng,
    };
    
    if(index == -1){
        currentBookingStops.push(stop);
    }else{
        currentBookingStops[index] = stop;
    }
    console.log(currentBookingStops);
}

function refreshMarkersLayer(data) {
    let features = [];

    let geojsonMarkers = {
        type: "geojson",
        data: {
      type: "FeatureCollection",
      features: features,
        },
    };
    
    data.forEach((marker) => {
        let feature = {
            type: "Feature",
            properties: {
                name: marker.name,
                address: marker.address,
            },
            geometry: {
                type: "Point",
                coordinates: [marker.lng, marker.lat],
            },
        };
        features.push(feature);
    });

    geojsonMarkers.data.features = features;

    drawMarkers(geojsonMarkers);
}

function drawMarkers(geojson) {
    if (typeof map.getLayer("markers") !== "undefined") {
        map.removeLayer("markers").removeSource("markers");
    }

    map.addSource("markers", geojson);

    map.addLayer({
        id: "markers",
        type: "circle",
        source: "markers",
        paint: {
            "circle-color": "#4264fb",
            "circle-radius": 10,
            "circle-stroke-width": 2,
            "circle-stroke-color": "#ffffff",
        },
    });
}

function book() {
    currentBooking.clearStops();
    let date = document.getElementById("datePicker").value;
    console.log(date)
    let taxiType = document.getElementById("taxiType").value;
    let taxi = taxiList.find((t) => t.type == taxiType && t.available);

    if (date == "") {
        alert("Please select the start time.");
    } else if (taxiType == "") {
        alert("Please select the taxi type.");
    } else if (currentBookingStops.length < 2) {
        alert("Please enter/select pick-up and/or drop-off location. ");
    } else if (taxi == null) {
    alert(
        `All ${taxiType} type of taxi are booked. Please select a different type.`
    );
    } else {
        currentBooking.pickupDateTime = date;
        currentBooking.taxiType = taxiType;
        currentBooking.taxi = taxi.rego;

        currentBooking.addStops(currentBookingStops);

        let onResponse = function (response) {
        console.log(response);
        currentBooking.distance = (
            parseFloat(currentBooking.distance) +
            parseFloat(response.routes[0].distance / 1000.0)
        ).toFixed(2);

        };
        currentBooking.getDistanceAndDuration(onResponse);

        setTimeout(() => {
            currentBooking.fare = calculateFare(
                taxiType,
                currentBooking.distance,
                date
            );

            trips.addTrip(currentBooking);
            
            if(confirm("Your booking starts at " +formatDateTime(currentBooking.pickupDateTime) + "\nTotal distance is " +currentBooking.distance + "KM and total cost is $" + currentBooking.fare)
            ){
                updateLocalStorageData(KEY_TRIPS, trips);
                currentBookingStops = [];
                updateAvailability(taxi.rego, !taxi.available);
                redirectToDetailPage(trips.length - 1);
            }else{
                window.location = "index.html";
            }
        }, 1000);
    }
    console.log(currentBooking);
};

//when book btn click, popup window show, then update LS and go to detail page.
function redirectToDetailPage(index) {
    updateLocalStorageData(KEY_TRIP_VIEW_INDEX, index);
    window.location = "tripDetail.html";
};


function removeStopFromPage(nthStop) {
    let id = "row" + nthStop;
    let stopToRemove = document.getElementById(id);
    
    document.getElementById("moreStops").removeChild(stopToRemove);
    
    let index = currentBookingStops.findIndex((t) => t._id == "stop" + nthStop);
    
    if(index != -1){
        currentBookingStops.splice(index, 1);
    }
    
    console.log(currentBookingStops);
};

document.getElementById("btnAddStops").addEventListener("click", function (e) {
    if (currentBookingStops.length >= 1) {
        nthStop += 1;
        addStopToPage(nthStop);
    }
});

document.getElementById("btnBook").addEventListener("click", book);

function clearThisPage(){
    if(confirm("Do You Want To CLEAR This Page?")){
        window.location.href="index.html";
    }
}















