"use strict";

updateLocalStorageData(KEY_TRIPS, trips);

let mapOfDetail;

let trip = trips.getTripByIndex(getLocalStorageData(KEY_TRIP_VIEW_INDEX));

let stops = trip._stops;
let numberOfStops = stops.length;

let tripIdByLS = trip._tripId;
let dateTimeByLS = trip._pickupDateTime;

let carTypeByLS = trip._taxiType;

let pickUpPointByLS = trip._stops[0]._name;
let destinationByLS = trip._stops[numberOfStops - 1]._name;

let stopsByLS = numberOfStops;
let distanceByLS = trip._distance;
let fareByLS = trip._fare;


let tripIdByHTML = document.getElementById("tripId");
let dateTimeByHTML = document.getElementById("dateTime");

let carTypeByHTML = document.getElementById("carType");
let pickUpPointByHTML = document.getElementById("pickUpPoint");
let destinationByHTML = document.getElementById("destination");

let stopsByHTML = document.getElementById("stops");
let distanceByHTML = document.getElementById("distance");
let fareByHTML = document.getElementById("fare");


//Mapbox access token
mapboxgl.accessToken = ACCESS_TOKEN;
//define the map
    mapOfDetail = new mapboxgl.Map({
    container: "mapOfDetail",
    style: "mapbox://styles/mapbox/streets-v11",
    zoom: 16,
    center: [stops[0]._lng, stops[0]._lat],
    });

function displayMarker(){

     for(let i = 0; i < numberOfStops; i++){
            
            new mapboxgl.Marker({
                color: "black",
            }).setLngLat([stops[i]._lng, stops[i]._lat])
            .setPopup(new mapboxgl.Popup({offset:5}).setHTML("<h7>"+ stops[i]._address +"<h7>")) 
            .addTo(mapOfDetail)
             new mapboxgl.Popup({offset:5})
            .setLngLat([stops[i]._lng, stops[i]._lat])
            .setHTML("<h7>"+ stops[i]._address +"<h7>")
            .addTo(mapOfDetail);
                
         //showRoute([stops[i]._lng, stops[i]._lat], [stops[i+1]._lng, stops[i+1]._lat])
        
     }
}

//addLayer 
//NOTE: this one is not work
function showRoute(start, end) {
    if (mapOfDetail.getLayer("route")) {
            mapOfDetail.removeLayer('route');
            mapOfDetail.removeSource("carRoute");
    }

    mapOfDetail.addSource('carRoute',{
        'type':"geojson",
        "data": {
            "type": "Feature",
            "properties": {},
            "geometry": {
            "type": "LineString",
            "coordinates": [start, end]
            }
        }  
    });

    mapOfDetail.addLayer({
        "id": "route",
        "source":"carRoute",
        "type": "line",

        "layer":{
            "type": "LineString",
        },

        "layout": {
            "line-join": "round",
            "line-cap": "round",
        },
        "paint": {
            "line-color": "red",
            "line-width": 2,
        }
    });
}


window.onload=function(){
    displayMarker();
}


//console.log(trip);

tripIdByHTML.innerHTML += tripIdByLS;
dateTimeByHTML.innerHTML += formatDateTime(dateTimeByLS);
    
carTypeByHTML.innerHTML += carTypeByLS ;

pickUpPointByHTML.innerHTML += pickUpPointByLS;
destinationByHTML.innerHTML += destinationByLS;
    
stopsByHTML.innerHTML += stopsByLS;
distanceByHTML.innerHTML += distanceByLS + 'KM';
fareByHTML.innerHTML += fareByLS + '$';


function closeDetailPage(){
    if(confirm("Do You Want To Close This Page?")){
        window.location.href="allTrip.html";
    }
}

function cancelThisPage(){ 
    if(confirm("Do You Want To DELETE This Trip?")){
        trips.removeTripById(tripIdByLS);
        updateLocalStorageData(KEY_TRIPS, trips);
        
        window.location.href="allTrip.html";
    }
}

function saveCarType(){
    if(confirm("Do You Want To Change The Type To Your Choice?")){
        //not finish
    }
}




