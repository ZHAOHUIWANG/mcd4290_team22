"use strict";
const ACCESS_TOKEN =
  "pk.eyJ1IjoiYW5zaHVvaHVhbmciLCJhIjoiY2tvaWp5c3Z0MG1zczJwcTR0ZHcycmppNSJ9.t7aH8G9OXS-9i4IlYwFEPw";

const KEY_TRIPS = "trips";
const KEY_TRIP_VIEW_INDEX = "trip_view_index";


class Trip {
  constructor() {
    this._tripId = generateUniqueId();
    this._pickupDateTime = "";
    this._taxiType = "";
    this._stops = [];
    this._taxi = "";
    this._distance = 0;
    this._dropoffDateTime = "";
    this._fare = 0;
  }

  set pickupDateTime(pickUpTime) {
    this._pickupDateTime = pickUpTime;
  }

  get pickupDateTime() {
    return this._pickupDateTime;
  }

  set dropoffDateTime(dropoffDateTime) {
    this._dropoffDateTime = dropoffDateTime;
  }

  get dropoffDateTime() {
    return this._dropoffDateTime;
  }

  set taxiType(taxiType) {
    this._taxiType = taxiType;
  }

  get taxiType() {
    return this._taxiType;
  }

  get taxi() {
    return this._taxiType;
  }

  set taxi(taxi) {
    this._taxi = taxi;
  }

  get fare() {
    return this._fare;
  }

  set fare(fare) {
    this._fare = fare;
  }

  get distance() {
    return this._distance;
  }

  set distance(distance) {
    this._distance = distance;
  }

  getDistanceAndDuration(onDistanceAndDurationResponse) {
    for (let i = 0; i < this._stops.length - 1; i++) {
      let currentStop = this._stops[i];
      let nextStop = this._stops[i + 1];
      sendXMLRequestForRoute(
        currentStop._lat,
        currentStop._lng,
        nextStop._lat,
        nextStop._lng,
        onDistanceAndDurationResponse
      );
    }
  }

  clearStops() {
    this._stops = [];
  }

  addStops(stops) {
    this._stops = stops;
  }

  addStop(stop) {
    this._stops.push(stop);
  }

  removeStopAtIndex(index) {
    this._stops.splice(index, 1);
  }

  fromData(data) {
    this._id = data._id;
    this._pickupDateTime = data._pickupDateTime;
    this._dropoffDateTime = data._dropoffDateTime;
    this.pickUpLocation = data._pickUpLocation;
    this.taxiType = data._taxiType;
    this.stops = data._stops;
    this.taxi = data._taxi;
    this.fare = data._fare;
    this.distance = data._distance;
  }
}

class Trips {
  constructor() {
    this._trips = [];
  }

  get length() {
    return this._trips.length;
  }

  getTripByIndex(index) {
    return this._trips[index];
  }

  getTripIndexById(id) {
    return this._trips.findIndex((t) => t._tripId == id);
  }

  removeTripById(id) {
    let index = this._trips.findIndex((t) => t._tripId == id);

    if (index != -1) {
      this._trips.splice(index, 1);
    }
  }

  addTrip(trip) {
    this._trips.push(trip);
  }

  filterTrips(condition) {
    return this._trips.filter(condition);
  }

  fromData(data) {
    let trips = [];

    data._trips.forEach((t) => {
      let trip = new Trip();
      let stops = [];
      trip._id = t._id;
      trip.pickupDateTime = t._pickupDateTime;
      trip.dropoffDateTime = t._dropoffDateTime;
      trip.taxiType = t._taxiType;
      trip.taxi = t._taxi;
      trip.fare = t._fare;
      trip.distance = t._distance;
      t._stops.forEach((s) => {
        stops.push(s);
      });
      trip._stops = stops;
      trips.push(trip);
    });

    this._trips = trips;
  }
}

function generateUniqueId() {
  return "Booking-NO.xxxxxx".replace(/[xy]/g, function (c) {
    var r = (Math.random() * 6) | 0,
      v = c == "x" ? r : (r & 0x3) | 0x3;
    return v.toString(6);
  });
}

function addSecondsToDateTime(millisec, date) {
  return formatDateTim(new Date(new Date(date).getTime() + millisec));
}

function formatDateTime(dateTimeString) {
  let date = new Date(dateTimeString);

  return date.toLocaleDateString() + " " + date.toLocaleTimeString();
}

//This function checks if data exists in localStorage by a given key
function checkLocalStorageDataExist(key) {
  if (localStorage.getItem(key)) {
    return true;
  }
  return false;
}

//This function updates the data into localStorage
//@param key - key of the data
//@param data - data to update
function updateLocalStorageData(key, data) {
  let jsonData = JSON.stringify(data);
  localStorage.setItem(key, jsonData);
}

//This function gets the data from localStorage by a given key
function getLocalStorageData(key) {
  let jsonData = localStorage.getItem(key);
  let data = jsonData;
  try {
    data = JSON.parse(jsonData);
  } catch (e) {
    console.error(e);
  } finally {
    return data;
  }
}

//code that will run when the file is loaded
let trips = new Trips();
let currentBooking = new Trip();
if (checkLocalStorageDataExist(KEY_TRIPS)) {
  // if LS data does exist
  let data = getLocalStorageData(KEY_TRIPS);
  trips.fromData(data);
}
