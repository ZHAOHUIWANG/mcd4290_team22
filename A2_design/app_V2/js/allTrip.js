"use strict";

//this function for allTrip.html view button
function viewTrip(id) {
  updateLocalStorageData(KEY_TRIP_VIEW_INDEX, trips.getTripIndexById(id));

  window.location.href = "tripDetail.html";
}

function deleteTrip(id) {
  if (confirm("Do You Want To DELETE This Trip?")) {
    trips.removeTripById(id);
    updateLocalStorageData(KEY_TRIPS, trips);

    displayTodayTrips();
    displayFutureTrips();
    displayPastTrips();
  }
}

function displayTodayTrips() {
  let todayTripInfo = trips.filterTrips(
    (t) => new Date(t._pickupDateTime).getDate() == new Date().getDate()
  );

  let html = "";

  todayTripInfo.forEach((t) => {
    html += `<li class="mdl-list__item mdl-list__item--three-line">
                <span class="mdl-list__item-primary-content">
                    <span>Trip ID: ${t._tripId}</span>
                </span>   
                <span class="mdl-list__item-text-body">
                    Trip Summary: Date / Time: ${formatDateTime(
                      t._pickupDateTime
                    )}.<br>Pick-Up Point: ${
      t._stops[0]._name
    } . The Final Destination: ${
      t._stops[t._stops.length - 1]._name
    }.<br> The Total Number of Stops:   ${
      t._stops.length
    }. Estimated Total Distance: ${
      t._distance === undefined ? 0 : t._distance
    }(KM) and Fare: ${t._fare === undefined ? 0 : t._fare}($)
                </span>   
                <span class="mdl-list__item-secondary-content">  
                    <a class="mdl-list__item-secondary-action" onclick="viewTrip('${
                      t._tripId
                    }')"><i
                        class="material-icons">info</i></a>
                </span>
                <span class="mdl-list__item-secondary-content">  
                    <a class="mdl-list__item-secondary-action" onclick="deleteTrip('${
                      t._tripId
                    }')"><i
                        class="material-icons">delete</i></a>
                </span>
            </li>`;
  });
  document.getElementById("todayTripInfo").innerHTML = html;
}

function displayPastTrips() {
  let pastTripInfo = trips.filterTrips(
    (t) => new Date(t._pickupDateTime) < new Date()
  );

  let html = "";

  pastTripInfo.forEach((t) => {
    html += `<li class="mdl-list__item mdl-list__item--three-line">
                <span class="mdl-list__item-primary-content">
                    <span>Trip ID: ${t._tripId}</span>
                </span>   
                <span class="mdl-list__item-text-body">
                    Trip Summary: Date / Time: ${formatDateTime(
                      t._pickupDateTime
                    )}.<br>Pick-Up Point: ${
      t._stops[0]._name
    } . The Final Destination: ${
      t._stops[t._stops.length - 1]._name
    }.<br> The Total Number of Stops:   ${
      t._stops.length
    }. Estimated Total Distance: ${
      t._distance === undefined ? 0 : t._distance
    }(KM) and Fare: ${t._fare === undefined ? 0 : t._fare}($)
                </span>   
                <span class="mdl-list__item-secondary-content">  
                    <a class="mdl-list__item-secondary-action" onclick="viewTrip('${
                      t._tripId
                    }')"><i
                        class="material-icons">info</i></a>
                </span>
                <span class="mdl-list__item-secondary-content">  
                    <a class="mdl-list__item-secondary-action" onclick="deleteTrip('${
                      t._tripId
                    }')"><i
                        class="material-icons">delete</i></a>
                </span>
            </li>`;
  });
  document.getElementById("pastTripInfo").innerHTML = html;
}

function displayFutureTrips() {
  let futureTripInfo = trips.filterTrips(
    (t) => new Date(t._pickupDateTime).getDate() >= new Date().getDate() + 1
  );

  let html = "";

  futureTripInfo.forEach((t) => {
    html += `<li class="mdl-list__item mdl-list__item--three-line">
                <span class="mdl-list__item-primary-content">
                    <span>Trip ID: ${t._tripId}</span>
                </span>   
                <span class="mdl-list__item-text-body">
                    Trip Summary: Date / Time: ${formatDateTime(
                      t._pickupDateTime
                    )}.<br>Pick-Up Point: ${
      t._stops[0]._name
    } . The Final Destination: ${
      t._stops[t._stops.length - 1]._name
    }.<br> The Total Number of Stops:   ${
      t._stops.length
    }. Estimated Total Distance: ${
      t._distance === undefined ? 0 : t._distance
    }(KM) and Fare: ${t._fare === undefined ? 0 : t._fare}($)
                </span>   
                <span class="mdl-list__item-secondary-content">  
                    <a class="mdl-list__item-secondary-action" onclick="viewTrip('${
                      t._tripId
                    }')"><i
                        class="material-icons">info</i></a>
                </span>
                <span class="mdl-list__item-secondary-content">  
                    <a class="mdl-list__item-secondary-action" onclick="deleteTrip('${
                      t._tripId
                    }')"><i
                        class="material-icons">delete</i></a>
                </span>
            </li>`;
  });
  document.getElementById("futureTripInfo").innerHTML = html;
}

window.onload = function () {
  //setInterval(, 1000);
  displayTodayTrips();
  displayFutureTrips();
  displayPastTrips();
};
