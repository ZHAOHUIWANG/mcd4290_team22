"use strict";
/**
File Header Documentation
    Name: main.js
    Purpose: 
        Create functionality for index.html. The code for this JS file runs only on index.html
**/

//Mapbox access token
mapboxgl.accessToken = ACCESS_TOKEN;
//define the map
let map;
map = new mapboxgl.Map({
  container: "map",
  style: "mapbox://styles/mapbox/streets-v11",
  zoom: 4,
  center: [134.09209, -26.083647],
});

map.addControl(
  new MapboxGeocoder({
    accessToken: mapboxgl.accessToken,
    mapboxgl: mapboxgl,
  })
);

let currentBookingStops = [];
let nthStop = 1;
let lat = 0;
let lng = 0;
let address = "";



//Get the current location of the user and show a popup & marker for usr current location

/**
name:
    getUserCurrentLocation
purpose:
    get usrer's current location's corrdinate
parameters:
    no parameters
return values:
    usrer's current location's corrdinate
**/
function getUserCurrentLocation() {
  if ("geolocation" in navigator) {
    navigator.geolocation.getCurrentPosition(success, error);
  } else {
    error();
  }
}

/**
name:
    success
purpose:
    after user's click on "yes" let the app get users current location
parameters:
    position
return values: user's current location
    
**/
function success(position) {
  lat = position.coords.latitude;
  lng = position.coords.longitude;

  sendWebServiceRequestForReverseGeocoding(lat, lng, "onAddressChanged");

  markPlaceOnMap();
}

/**
name:
    error
purpose:
    to show the text "It requires user's permission to use this feature"
parameters: 
    no parameter
return values: It requires user's permission to use this feature
    
**/
function error() {
  alert("It requires user's permission to use this feature");
}

/**
name:
    markPlaceOnMap
purpose:
    when user click on a point on the map, its address will show up
parameters:
    lng, lat
return values: 
    The point's adress and "add" button
**/
function markPlaceOnMap() {
    map.flyTo({
        center: [lng, lat],
        essential: true,
        zoom: 15,
      });
  
      setTimeout(function (e) {
        let popup = new mapboxgl.Popup({ closeOnClick: false })
          .setLngLat([lng, lat])
          .setHTML(
            `<p><strong>${address != "" ? address.substring(
              0,
              address.indexOf(",")
            ) : `Your start location`}<br/></strong>${address}</p>
            <button id="deletestop${nthStop}" class="btnDeleteLocation" onclick="onDeleteLocation('${
              stop._id
            }')">Delete</button>
          <button id="stop${nthStop}" class="btnAdd" onclick="onAddLocation(${lat}, ${lng}, '${
              stop._id
            }')">Add</button>
            `
          )
          .addTo(map);
  
        let marker = new mapboxgl.Marker({ draggable: true })
          .setLngLat([lng, lat])
          .setPopup(popup)
          .addTo(map)
          .togglePopup();
        //   console.log(nthStop);
        function onDragEnd() {
          let lngLat = marker.getLngLat();
          lat = lngLat.lat;
          lng = lngLat.lng;
  
          sendWebServiceRequestForReverseGeocoding(lat, lng, "onAddressChanged");
  
          setTimeout(function () {
            popup.setHTML(
              `<p><strong>${address.substring(
                0,
                address.indexOf(",")
              )}</strong><br/>${address}</p>
              <button id="deletestop${nthStop}" class="btnDeleteLocation" onclick="onDeleteLocation('${
                stop._id
              }')">Delete</button>
              <button id="stop${nthStop}" class="btnAdd" onclick="onAddLocation(${lat},${lng},'${
                stop._id
              }')">Add</button>
             `
            );
  
            document
              .getElementById(`stop${nthStop}`)
              .addEventListener("click", function (e) {
                console.log(nthStop);
  
                let ref = document.getElementById(`stop${nthStop - 1}`);
                if (ref !== null) {
                  ref.style.visibility = "hidden";
                }
                ref= document.getElementById(`stop${nthStop}`);
                if (ref !== null) {
                 ref.style.visibility = "hidden";
               }
              });
            document
              .getElementById(`deletestop${nthStop}`)
              .addEventListener("click", function (e) {
                onDeleteLocation(stop._id);
                marker.remove();
              });
          }, 1000);
        }
  
        marker.on("dragend", onDragEnd);
  
        document
          .getElementById(`stop${nthStop}`)
          .addEventListener("click", function (e) {
            let ref = document.getElementById(`stop${nthStop - 1}`);
            if (ref !== null) {
              ref.style.visibility = "hidden";
            }
            ref= document.getElementById(`stop${nthStop}`);
            if (ref !== null) {
             ref.style.visibility = "hidden";
           }
          });
        document
          .getElementById(`deletestop${nthStop}`)
          .addEventListener("click", function (e) {
            onDeleteLocation(stop._id);
            marker.remove();
          });
      }, 1000);
}

/**
name:
    onAddressChanged
purpose:
    get the value of coordinate and add to the address
parameters:
    response
return values:
    address
**/
function onAddressChanged(response) {
  let result = response.results[0];
  //   lat = result.geometry.lat;
  //   lng = result.geometry.lng;
  address = result.formatted;
}


/**
name:
    drawPath
purpose:
    draw a line between tow points
parameters:
    data
return values:
   the path between two points
    
**/
function drawPath(data) {
  let features = [];

  let geojson = {
    type: "geojson",
    data: {
      type: "FeatureCollection",
      features: features,
    },
  };

  for (let i = 0; i < data.length - 1; i++) {
    let feature = {
      type: "Feature",
      properties: {
        name: data[i]._name,
        address: data[i]._address,
      },
      geometry: {
        type: "LineString",
        coordinates: [
          [data[i]._lng, data[i]._lat],
          [data[i + 1]._lng, data[i + 1]._lat],
        ],
      },
    };
    features.push(feature);
  }

  geojson.data.features = features;

  if (typeof map.getLayer("routes") !== "undefined") {
    map.removeLayer("routes").removeSource("routes");
  }
  map.addSource("routes", geojson);
  map.addLayer({
    id: "routes",
    type: "line",
    source: "routes",
    paint: {
      "line-color": "#555",
      "line-width": 4,
    },
  });
}

/**
name:
    book
purpose:
    get a list of information including all the address, paths, starting time, type of vehicle, and total cost
parameters:
    
return values:
    A list of information including all the address, paths, starting time, type of vehicle, and total cost
**/
function book() {
  currentBooking.clearStops();
  let date = document.getElementById("datePicker").value;
  console.log(date);
  let taxiType = document.getElementById("taxiType").value;
  let taxi = taxiList.find((t) => t.type == taxiType && t.available);

  if (date == "") {
    alert("Please select the start time.");
  } else if (taxiType == "") {
    alert("Please select the taxi type.");
  } else if (currentBookingStops.length < 2) {
    alert("Please enter/select pick-up and/or drop-off location. ");
  } else if (taxi == null) {
    alert(
      `All ${taxiType} type of taxi are booked. Please select a different type.`
    );
  } else {
    currentBooking.pickupDateTime = date;
    currentBooking.taxiType = taxiType;
    currentBooking.taxi = taxi.rego;

    currentBooking.addStops(currentBookingStops);

    let onResponse = function (response) {
      console.log(response);
      currentBooking.distance = (
        parseFloat(currentBooking.distance) +
        parseFloat(response.routes[0].distance / 1000.0)
      ).toFixed(2);
    };
    currentBooking.getDistanceAndDuration(onResponse);

    setTimeout(() => {
      currentBooking.fare = calculateFare(
        taxiType,
        currentBooking.distance,
        date
      );

      trips.addTrip(currentBooking);

      if (
        confirm(
          "Your booking starts at " +
            formatDateTime(currentBooking.pickupDateTime) +
            "\nTotal distance is " +
            currentBooking.distance +
            "KM and total cost is $" +
            currentBooking.fare
        )
      ) {
        updateLocalStorageData(KEY_TRIPS, trips);
        currentBookingStops = [];
        updateAvailability(taxi.rego, !taxi.available);
        redirectToDetailPage(trips.length - 1);
      } else {
        window.location = "index.html";
      }
    }, 1000);
  }
  console.log(currentBooking);
}


/**
name:
    redirectToDetailPage
purpose:
    when book btn click, popup window show, then update LS and go to detail page.
parameters:
    index
return values:
    no return
**/
function redirectToDetailPage(index) {
  updateLocalStorageData(KEY_TRIP_VIEW_INDEX, index);
  window.location = "tripDetail.html";
}


/**
name:
    removeStopFromPage
purpose:
    To delete a stop from the page
parameters:
    nthStop
return values:
    no return
**/
function removeStopFromPage(nthStop) {
  let id = "row" + nthStop;
  let stopToRemove = document.getElementById(id);

  document.getElementById("moreStops").removeChild(stopToRemove);

  let index = currentBookingStops.findIndex((t) => t._id == "stop" + nthStop);

  if (index != -1) {
    currentBookingStops.splice(index, 1);
  }

  console.log(currentBookingStops);
}


document.getElementById("btnBook").addEventListener("click", book);

/**
name:
    clearThisPage
purpose:
    clear this page
parameters:
    no parameter
return values:
    no return
**/
function clearThisPage() {
  if (confirm("Do You Want To CLEAR This Page?")) {
    window.location.href = "index.html";
  }
}

/**
name:
    onAddLocation
purpose: Add button on marks's popup window, the path bitween this stop and the previous point will be showed up. A callback function dealing the API
    
parameters:
    lat, lng, id
return values:
    
**/
function onAddLocation(lat, lng, id = "") {
  let index = currentBookingStops.findIndex((b) => b._id == id);
  let stop = {
    _id: id,
    _name: address.substring(0, address.indexOf(",")),
    _address: address,
    _lat: lat,
    _lng: lng,
  };

  if (index == -1) {
    currentBookingStops.push(stop);
    nthStop += 1;
  } else {
    currentBookingStops[index] = stop;
  }
  console.log(currentBookingStops);
  drawPath(currentBookingStops);
}

/**
name:
    onDeleteLocation
purpose: delete button on mark's popup window. A callback function dealing the API
    
parameters:
    id
return values:
    
**/
function onDeleteLocation(id) {
  let index = currentBookingStops.findIndex((b) => b._id == id);
  if (index != -1) {
    currentBookingStops.splice(index, 1);
  }
  console.log(currentBookingStops);
  drawPath(currentBookingStops);
}

/**
name:
    No name
purpose: 
    When user cklik on a point on the map, the coordinate will be showed up
parameters:
    lat lng
return values: 
    
**/
map.on("load", function () {
  map.on("click", function (e) {
    lat = e.lngLat.lat;
    lng = e.lngLat.lng;

    let stop = {
      _id: "stop" + nthStop,
    };

    sendWebServiceRequestForReverseGeocoding(lat, lng, "onAddressChanged");

    map.flyTo({
      center: [lng, lat],
      essential: true,
      zoom: 15,
    });

    setTimeout(function (e) {
      let popup = new mapboxgl.Popup({ closeOnClick: false })
        .setLngLat([lng, lat])
        .setHTML(
          `<p><strong>${address != "" ? address.substring(
            0,
            address.indexOf(",")
          ) : `Your start location`}<br/></strong>${address}</p>
          <button id="deletestop${nthStop}" class="btnDeleteLocation" onclick="onDeleteLocation('${
            stop._id
          }')">Delete</button>
        <button id="stop${nthStop}" class="btnAdd" onclick="onAddLocation(${lat}, ${lng}, '${
            stop._id
          }')">Add</button>
          `
        )
        .addTo(map);

      let marker = new mapboxgl.Marker({ draggable: true })
        .setLngLat([lng, lat])
        .setPopup(popup)
        .addTo(map)
        .togglePopup();
      //   console.log(nthStop);
      function onDragEnd() {
        let lngLat = marker.getLngLat();
        lat = lngLat.lat;
        lng = lngLat.lng;

        sendWebServiceRequestForReverseGeocoding(lat, lng, "onAddressChanged");

        setTimeout(function () {
          popup.setHTML(
            `<p><strong>${address.substring(
              0,
              address.indexOf(",")
            )}</strong><br/>${address}</p>
            <button id="deletestop${nthStop}" class="btnDeleteLocation" onclick="onDeleteLocation('${
              stop._id
            }')">Delete</button>
            <button id="stop${nthStop}" class="btnAdd" onclick="onAddLocation(${lat},${lng},'${
              stop._id
            }')">Add</button>
           `
          );

          document
            .getElementById(`stop${nthStop}`)
            .addEventListener("click", function (e) {
              console.log(nthStop);

              let ref = document.getElementById(`stop${nthStop - 1}`);
              console.log(ref)
              if (ref !== null) {
                ref.style.visibility = "hidden";
              }

             ref= document.getElementById(`stop${nthStop}`);
             if (ref !== null) {
              ref.style.visibility = "hidden";
            }
            });
          document
            .getElementById(`deletestop${nthStop}`)
            .addEventListener("click", function (e) {
              onDeleteLocation(stop._id);
              marker.remove();
            });
        }, 1000);
      }

      marker.on("dragend", onDragEnd);

      document
        .getElementById(`stop${nthStop}`)
        .addEventListener("click", function (e) {
          let ref = document.getElementById(`stop${nthStop - 1}`);
          if (ref !== null) {
            ref.style.visibility = "hidden";
          }
          ref= document.getElementById(`stop${nthStop}`);
          if (ref !== null) {
           ref.style.visibility = "hidden";
         }
        });
      document
        .getElementById(`deletestop${nthStop}`)
        .addEventListener("click", function (e) {
          onDeleteLocation(stop._id);
          marker.remove();
        });
    }, 1000);
  });
});


