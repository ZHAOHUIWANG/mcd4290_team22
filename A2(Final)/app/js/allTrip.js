"use strict";
/**
File Header Documentation
    Name: allTrip.js
    Purpose: 
        Create functionality for allTrip.html. The code for this JS file runs only on allTrip.html
**/


//this function for allTrip.html view button

/**
name:
    viewTrip
purpose:
    Turn to the pagr "tripDetail.html"
parameters:
    id
return values:
    no return
**/
function viewTrip(id) {
  updateLocalStorageData(KEY_TRIP_VIEW_INDEX, trips.getTripIndexById(id));

  window.location.href = "tripDetail.html";
}

/**
name:
    deleteTrip
purpose:
    when user click on yes on the popup window that ask "Do You Want To DELETE This Trip?", the trip will be deleted
parameters:
    id
return values:
    no return
**/
function deleteTrip(id) {
  if (confirm("Do You Want To DELETE This Trip?")) {
    trips.removeTripById(id);
    updateLocalStorageData(KEY_TRIPS, trips);

    displayTodayTrips();
    displayFutureTrips();
    displayPastTrips();
  }
}

/**
name:
    displayTodayTrips
purpose: 
    Display the all the information of the trip that has same date of starting time as today
parameters: 
    no parameter
return values: 
    no return
**/
function displayTodayTrips() {
  let todayTripInfo = trips.filterTrips(
    (t) => new Date(t._pickupDateTime).getDate() == new Date().getDate()
  );

  let html = "";

  todayTripInfo.forEach((t) => {
    html += `
        <li class="mdl-list__item mdl-list__item--three-line">
            <span class="mdl-list__item-primary-content">
                <span>Trip ID: ${t._tripId}</span>
            </span>   
            <span class="mdl-list__item-text-body">
                Trip Summary: Date / Time: ${
                    formatDateTime(t._pickupDateTime)
                }.<br>Pick-Up Point: ${
                    t._stops[0]._name
                } . The Final Destination: ${
                    t._stops[t._stops.length - 1]._name
                }.<br> The Total Number of Stops:   ${
                    t._stops.length
                }. Estimated Total Distance: ${
                    t._distance === undefined ? 0 : t._distance
                }(KM) and Fare: ${t._fare === undefined ? 0 : t._fare}($)
            </span>   
            <span class="mdl-list__item-secondary-content">  
                <a class="mdl-list__item-secondary-action" onclick="viewTrip('${
                    t._tripId
                }')">
                <i class="material-icons">info</i></a>
            </span>
            <span class="mdl-list__item-secondary-content">  
                <a class="mdl-list__item-secondary-action" onclick="deleteTrip('${
                    t._tripId
                }')">
                <i class="material-icons">delete</i></a>
            </span>
        </li>`;
  });
  document.getElementById("todayTripInfo").innerHTML = html;
}

/**
name:
    displayPastTrips
purpose:
    Display the all the information of the trip that has earlier date of starting time as today
parameters:
    no parameter
return values: 
    no return
**/
function displayPastTrips() {
  let pastTripInfo = trips.filterTrips(
    (t) => new Date(t._pickupDateTime) < new Date()
  );

  let html = "";

  pastTripInfo.forEach((t) => {
    html += `
        <li class="mdl-list__item mdl-list__item--three-line">
            <span class="mdl-list__item-primary-content">
                <span>Trip ID: ${t._tripId}</span>
            </span>   
            <span class="mdl-list__item-text-body">
                Trip Summary: Date / Time: ${
                    formatDateTime(t._pickupDateTime)
                }.<br>Pick-Up Point: ${
                    t._stops[0]._name
                } . The Final Destination: ${
                    t._stops[t._stops.length - 1]._name
                }.<br> The Total Number of Stops:   ${
                    t._stops.length
                }. Estimated Total Distance: ${
                    t._distance === undefined ? 0 : t._distance
                }(KM) and Fare: ${t._fare === undefined ? 0 : t._fare}($)
            </span>   
            <span class="mdl-list__item-secondary-content">  
                <a class="mdl-list__item-secondary-action" onclick="viewTrip('${
                    t._tripId
                }')">
                <i class="material-icons">info</i></a>
            </span>
            <span class="mdl-list__item-secondary-content">  
                <a class="mdl-list__item-secondary-action" onclick="deleteTrip('${
                    t._tripId
                }')">
                <i class="material-icons">delete</i></a>
            </span>
        </li>`;
  });
  document.getElementById("pastTripInfo").innerHTML = html;
}

/**
name:
    displayFutureTrips
purpose: 
    Display the all the information of the trip that has later date of starting time as today
parameters:
    No parameter
return values: 
    No return
**/
function displayFutureTrips() {
  let futureTripInfo = trips.filterTrips(
    (t) => new Date(t._pickupDateTime).getDate() >= new Date().getDate() + 1
  );

  let html = "";

  futureTripInfo.forEach((t) => {
    html += `
        <li class="mdl-list__item mdl-list__item--three-line">
            <span class="mdl-list__item-primary-content">
                <span>Trip ID: ${t._tripId}</span>
            </span>   
            <span class="mdl-list__item-text-body">
                Trip Summary: Date / Time: ${
                    formatDateTime(t._pickupDateTime)
                }.<br>Pick-Up Point: ${
                    t._stops[0]._name
                } . The Final Destination: ${
                    t._stops[t._stops.length - 1]._name
                }.<br> The Total Number of Stops:   ${
                    t._stops.length
                }. Estimated Total Distance: ${
                    t._distance === undefined ? 0 : t._distance
                }(KM) and Fare: ${t._fare === undefined ? 0 : t._fare}($)
            </span>   
            <span class="mdl-list__item-secondary-content">  
                <a class="mdl-list__item-secondary-action" onclick="viewTrip('${
                    t._tripId
                }')">
                <i class="material-icons">info</i></a>
            </span>
            <span class="mdl-list__item-secondary-content">  
                <a class="mdl-list__item-secondary-action" onclick="deleteTrip('${
                    t._tripId
                }')">
                <i class="material-icons">delete</i></a>
            </span>
        </li>`;
  });
  document.getElementById("futureTripInfo").innerHTML = html;
}

/**
name:
    No name
purpose:
    when allTrip.html are on load, these three function is running.
parameters: 
    no parameter
return values: 
    no return
**/
window.onload = function () {
  displayTodayTrips();
  displayFutureTrips();
  displayPastTrips();
};
