"use strict";
/**
File Header Documentation
    Name: share.js
    Purpose: 
        The shared.js file is loaded on all pages and defines two classes (Trip and Trips).
        Share code to all files. For example, load code for a trial run on a web page.
        Three constants provide keys for local Storage.
**/


const ACCESS_TOKEN =
  "pk.eyJ1IjoiYW5zaHVvaHVhbmciLCJhIjoiY2tvaWp5c3Z0MG1zczJwcTR0ZHcycmppNSJ9.t7aH8G9OXS-9i4IlYwFEPw";

const KEY_TRIPS = "trips";
const KEY_TRIP_VIEW_INDEX = "trip_view_index";

//Class
class Trip {
    constructor() {
        this._tripId = generateUniqueId();
        this._pickupDateTime = "";
        this._taxiType = "";
        this._stops = [];
        this._taxi = "";
        this._distance = 0;
        this._dropoffDateTime = "";
        this._fare = 0;
    }

    //accessors
    set pickupDateTime(pickUpTime) {
        this._pickupDateTime = pickUpTime;
    }

    get pickupDateTime() {
        return this._pickupDateTime;
    }

    set dropoffDateTime(dropoffDateTime) {
        this._dropoffDateTime = dropoffDateTime;
    }

    get dropoffDateTime() {
        return this._dropoffDateTime;
    }

    set taxiType(taxiType) {
        this._taxiType = taxiType;
    }

    get taxiType() {
    return this._taxiType;
    }

    get taxi() {
        return this._taxiType;
    }

    set taxi(taxi) {
        this._taxi = taxi;
    }

    get fare() {
    return this._fare;
    }

    set fare(fare) {
        this._fare = fare;
    }

    get distance() {
        return this._distance;
    }

    set distance(distance) {
        this._distance = distance;
    }

    //methods
    //Count the distance between the two stops
    getDistanceAndDuration(onDistanceAndDurationResponse) {
        for (let i = 0; i < this._stops.length - 1; i++) {
            let currentStop = this._stops[i];
            let nextStop = this._stops[i + 1];
            sendXMLRequestForRoute(
                currentStop._lat,
                currentStop._lng,
                nextStop._lat,
                nextStop._lng,
                onDistanceAndDurationResponse
            );
        }
    }

    //Remove all stops
    clearStops() {
        this._stops = [];
    }

    //add stops
    addStops(stops) {
        this._stops = stops;
    }

    //Add stops to array
    addStop(stop) {
        this._stops.push(stop);
    }

    //Remove specific stops
    removeStopAtIndex(index) {
        this._stops.splice(index, 1);
    }

    //Add JSON data from local Storage back to the student class.
    fromData(data) {
        this._id = data._id;
        this._pickupDateTime = data._pickupDateTime;
        this._dropoffDateTime = data._dropoffDateTime;
        this.pickUpLocation = data._pickUpLocation;
        this.taxiType = data._taxiType;
        this.stops = data._stops;
        this.taxi = data._taxi;
        this.fare = data._fare;
        this.distance = data._distance;
    }
}

//Class
class Trips {
    constructor() {
        this._trips = [];
    }

    //accessors
    get length() {
        return this._trips.length;
    }

    //methods
    //Find a specific trip by a specific index
    getTripByIndex(index) {
        return this._trips[index];
    }

    ////Find a specific trip index by a specific trip ID
    getTripIndexById(id) {
        return this._trips.findIndex((t) => t._tripId == id);
    }

    //remove a trip by a specific ID
    removeTripById(id) {
        let index = this._trips.findIndex((t) => t._tripId == id);

        if (index != -1) {
            this._trips.splice(index, 1);
        }
    }

    //remove a trip by a specific index
    removeTripByIndex(index){
        this._trips.splice(index, 1);
    }

    //add a trip to Trips array
    addTrip(trip) {
        this._trips.push(trip);
    }

    //update a trip by specific index
    updateTrip(index, trip){
        this._trips[index] = trip;
    }

    //Filter specific trips by specific condition
    filterTrips(condition) {
        return this._trips.filter(condition);
    }

    //Add JSON data from local Storage back to the student class.
    fromData(data) {
        let trips = [];

        data._trips.forEach((t) => {
            let trip = new Trip();
            let stops = [];
            trip._id = t._id;
            trip.pickupDateTime = t._pickupDateTime;
            trip.dropoffDateTime = t._dropoffDateTime;
            trip.taxiType = t._taxiType;
            trip.taxi = t._taxi;
            trip.fare = t._fare;
            trip.distance = t._distance;
            t._stops.forEach((s) => {
            stops.push(s);
            });
            trip._stops = stops;
            trips.push(trip);
        });

        this._trips = trips;
    }
}

/**
name:
    generateUniqueId
purpose:
    Generate a random 6-digit trip ID
parameters:
    No
return values:
    Random six-digit number
**/
function generateUniqueId() {
  return "Booking-NO.xxxxxx".replace(/[xy]/g, function (c) {
    var r = (Math.random() * 6) | 0,
      v = c == "x" ? r : (r & 0x3) | 0x3;
    return v.toString(6);
  });
}

/**
name:
    addSecondsToDateTime
purpose:
    Add seconds to time
parameters:
    millisec, date
return values:
    Time containing seconds
**/
function addSecondsToDateTime(millisec, date) {
  return formatDateTim(new Date(new Date(date).getTime() + millisec));
}

/**
name:
    formatDateTime
purpose:
    Format the date and time
parameters:
    dateTimeString
return values:
    Date and time after formatting
**/
function formatDateTime(dateTimeString) {
  let date = new Date(dateTimeString);

  return date.toLocaleDateString() + " " + date.toLocaleTimeString();
}

/**
name: 
    checkLocalStorageDataExist
purpose: 
    This task will check whether the browser can use local storage.
parameters: 
    key of the local storage.
return values:
    Boolean(true or false)
**/
function checkLocalStorageDataExist(key) {
  if (localStorage.getItem(key)) {
    return true;
  }
  return false;
}

/**
name: 
    updateLocalStorageData
purpose: 
    Quickly update data for a specific key in a Local storage
parameters: 
    key: The data of the key
    data: The data you want to upload
return values:
    If no value is returned, you can check the data of the application in the browser's developer tools
**/
function updateLocalStorageData(key, data) {
  let jsonData = JSON.stringify(data);
  localStorage.setItem(key, jsonData);
}

/**
name:
    getLocalStorageData
purpose:
    Retrieves data for a specific key in the local storage
parameters:
    key: The data of the key
return values:
    Data that is parsed and restored to an object.
**/
function getLocalStorageData(key) {
  let jsonData = localStorage.getItem(key);
  let data = jsonData;
  try {
    data = JSON.parse(jsonData);
  } catch (e) {
    console.error(e);
  } finally {
    return data;
  }
}

//code that will run when the file is loaded
let trips = new Trips();
let currentBooking = new Trip();
if (checkLocalStorageDataExist(KEY_TRIPS)) {
  // if LS data does exist
  let data = getLocalStorageData(KEY_TRIPS);
  trips.fromData(data);
}
