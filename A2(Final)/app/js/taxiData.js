let taxiList = [
  { rego: "VOV-887", type: "Car", available: true },
  { rego: "OZS-293", type: "Van", available: false },
  { rego: "WRE-188", type: "SUV", available: true },
  { rego: "FWZ-490", type: "Car", available: true },
  { rego: "NYE-874", type: "SUV", available: true },
  { rego: "TES-277", type: "Car", available: false },
  { rego: "GSP-874", type: "SUV", available: false },
  { rego: "UAH-328", type: "Minibus", available: true },
  { rego: "RJQ-001", type: "SUV", available: false },
  { rego: "AGD-793", type: "Minibus", available: false },
];



function calculateFare(taxiType, distance, startDateTime) {
  let faltRate = 4.2;
  let distanceRate = 1.622;
  let vehicleRate = getVehicleLevy(taxiType);
  let startTime = new Date(startDateTime).getHours();
  console.log(distance)
  let totalFare = faltRate + distance * distanceRate + vehicleRate;
  if (startTime > 17 && startTime < 21) {
    totalFare *= 1.2;
  }
  return totalFare.toFixed(2);
}

function getVehicleLevy(taxiType) {
  let levy = 0;
  if (taxiType == "Car") {
    levy = 1.1;
  } else if (taxiType == "SUV") {
    levy = 3.5;
  } else if (taxiType == "Van") {
    levy = 6;
  } else if (taxiType == "Minibus") {
    levy = 10;
  }
  return levy;
}

function updateAvailability(rego, newValue) {
  for (let i in taxiList) {
    if (taxiList[i].rego == rego) {
      taxiList[i].available = newValue;
      break;
    }
  }
}



