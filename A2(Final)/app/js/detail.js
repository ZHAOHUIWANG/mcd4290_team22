"use strict";
/**
File Header Documentation
    Name: detail.js
    Purpose: 
        Create functionality for tripDetail.html. The code for this JS file runs only on tripDetail.html
**/

let mapOfDetail;
let index = getLocalStorageData(KEY_TRIP_VIEW_INDEX);
let trip = trips.getTripByIndex(index);

let stops = trip._stops;
let numberOfStops = stops.length;

//Mapbox access token
mapboxgl.accessToken = ACCESS_TOKEN;
//define the map
mapOfDetail = new mapboxgl.Map({
  container: "mapOfDetail",
  style: "mapbox://styles/mapbox/streets-v11",
  zoom: 15,
  center: [stops[0]._lng, stops[0]._lat],
});

/**
name:
    displayMarker
purpose:
    Put the marker and popup of all stops for a particular trip on the map
parameters:
    lng and lat of stops
return values:
    No return
**/
function displayMarker() {
  for (let i = 0; i < numberOfStops; i++) {
    let popup = new mapboxgl.Popup()
      .setLngLat([stops[i]._lng, stops[i]._lat])
      .setHTML("<h7>" + stops[i]._address + "<h7>")
      .addTo(mapOfDetail);

    new mapboxgl.Marker({
      color: "black",
    })
      .setLngLat([stops[i]._lng, stops[i]._lat])
      .setPopup(popup)
      .addTo(mapOfDetail)
      .togglePopup();
  }
}

//addLayer

/**
name:
    showPath
purpose:
    Show the routes between markers
parameters:
    data
return values:
    No return
**/
function showPath(data) {
  let features = [];

  let geojson = {
    type: "geojson",
    data: {
      type: "FeatureCollection",
      features: features,
    },
  };

  for (let i = 0; i < data.length - 1; i++) {
    let feature = {
      type: "Feature",
      properties: {
        name: data[i]._name,
        address: data[i]._address,
      },
      geometry: {
        type: "LineString",
        coordinates: [
          [data[i]._lng, data[i]._lat],
          [data[i + 1]._lng, data[i + 1]._lat],
        ],
      },
    };
    features.push(feature);
  }

  geojson.data.features = features;

  if (typeof mapOfDetail.getLayer("routes") !== "undefined") {
    mapOfDetail.removeLayer("routes").removeSource("routes");
  }
  mapOfDetail.addSource("routes", geojson);
  mapOfDetail.addLayer({
    id: "routes",
    type: "line",
    source: "routes",
    paint: {
      "line-color": "#555",
      "line-width": 4,
    },
  });
}

/**
name:
    No name
purpose:
    When the Detail page loads, function named displayMarker() and function named showPath() will run.
parameters:
    No parameters
return values:
    No return
**/
mapOfDetail.on("load", function (e) {
  displayMarker();
  showPath(stops);
});

/**
name:
    displayBookingDetails
purpose:
    Displays all information for a specific trip
parameters:
    trip
return values:
    No return
**/
function displayBookingDetails(trip) {
  console.log(trip);
  let html = `
    <li><strong>Trip ID:</strong><span>${trip._tripId}</span></li>
    <li><strong>Start Date / Time:</strong><span>${formatDateTime(trip.pickupDateTime)}</span></li>
    <li><strong>Taxi Type: </strong>
          <select class="mdl-textfield__input" id="taxiType" name="taxiType" onchange="onTypeChange()">
                <option value=""></option>
                <option value="Car">Car</option>
                <option value="Van">Van</option>
                <option value="SUV">SUV</option>
                <option value="Minibus">Minibus</option>
          </select>
    </li>
    <li><strong>Pick-up point:</strong><span>${
      stops[0]._name
    }</span></li>
    <li><strong>Drop-off point:</strong><span>${
      stops[numberOfStops - 1]._name
    }</span></li>
    <li><strong>No. of Stops:</strong><span>${
      stops.length
    }</span></li>

    <li><strong>Stops:</strong>
        <li id="displayAllStops"></li>
    </li>

    <li><strong>Total Distance:</strong><span>${
      trip._distance
    } </span>KM</li>
    <li><strong>Total Fare:</strong><span id="fare">${
      trip._fare
    }$</span></li>
    <li>
        <button id="btnCancel" onclick="cancelBooking()" class=" mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">
            Cancel
        </button>
        <button id="btnUpdate" onclick="updateBooking(index, trip)" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect">
            Update
        </button>
        <br><br>
        <button id="closeDetailPage" onclick="closeDetailPage()" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect"><!--disabled -->
            Close This Page
        </button>
    </li>
`;

    document.getElementById("tripDetails").innerHTML = html;
    document.getElementById("taxiType").value = trip._taxiType;
  
    if (!isFutureTrip(trip.pickupDateTime)) {
        resetStatus();
    }
    
    displayAllStops();
}

/**
name:
    isFutureTrip
purpose:
    Determine if it's a trip in the future
parameters:
    pickupDateTime
return values:
    Time and date of future trip
**/
function isFutureTrip(pickupDateTime) {
  return new Date(pickupDateTime).getDate() > new Date().getDate();
}

/**
name:
    resetStatus
purpose:
    Fix the car type and hide the two buttons
parameters:
    No parameters
return values:
    No return
**/
function resetStatus() {
    document.getElementById("taxiType").style.pointerEvents = "none";
    document.getElementById("btnUpdate").style.visibility = "hidden";
    document.getElementById("btnCancel").style.visibility = "hidden";
}

/**
name:
    onTypeChange
purpose:
    The cost will change when the type of car changes
parameters:
    type
return values:
    No return
**/
function onTypeChange() {
    let type = document.getElementById("taxiType").value;
    let taxi = taxiList.find((t) => t.type == type && t.available);
  
    if (taxi != undefined) {
        let fare = calculateFare(type, trip._distance, trip._pickupDateTime);
        
        document.getElementById("fare").innerText = fare + '$';
        
        trip.taxi = taxi;
        trip.fare = fare;
    } else {
        alert("No available taxi under this type");
    }
}

/**
name:
    cancelBooking
purpose:
    Delete specific trips
parameters:
     index, trip
return values:
    No return.
**/
function cancelBooking() {
    if (confirm("Do you really want to cancel this trip?")) {
        let index = getLocalStorageData(KEY_TRIP_VIEW_INDEX);
        let trip = trips.getTripByIndex(index);
        
        updateAvailability(trip._taxi);
        trips.removeTripByIndex(index);
        updateLocalStorageData(KEY_TRIPS, trips);
        
        window.location = "index.html";
    }
}

/**
name:
    updateBooking
purpose:
    Updates LS information about a trip when a specific trip changes
parameters:
    index, trip
return values:
    No return
**/
function updateBooking(index, trip) {
    if (confirm("Do you want to change your car type? (NOTE: The fare is different for different car type)")) {
    trip.taxiType = document.getElementById("taxiType").value;
    trips.updateTrip(index, trip);
    updateLocalStorageData(KEY_TRIPS, trips);
    
    alert("Trip updated.");
    }
}

/**
name:
    closeDetailPage
purpose:
    Close the specific Trip detail Page and jump to the All Trip Page
parameters:
    No parameters
return values:
    No return
**/
function closeDetailPage(){
    if(confirm("Do You Want To Close This Page?")){
        window.location.href="allTrip.html";
    }
}

/**
name:
    displayAllStops
purpose:
    Display the names of all stops
parameters:
    No parameters
return values:
    No return
**/
function displayAllStops(){
    let displayStops = '';
    
    displayStops += stops[0]._name;
    
    for(let i = 1; i < numberOfStops; i++){
        displayStops += " --> " + trip._stops[i]._name;
    }
    
    document.getElementById("displayAllStops").innerHTML = displayStops;
}

displayBookingDetails(trip);

